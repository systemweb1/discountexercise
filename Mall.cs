﻿using discountexercise.AddProduct;
using DiscountLibrary;
using DiscountLibrary.Category;

internal class Mall
{
    private Invoice _invoice;
    private CartManage _cartManage;
    private Receipt _receipt;

    public Mall(Invoice invoice, CartManage cartManage)
    {
        _invoice = invoice;
        _cartManage = cartManage;
        _receipt = new Receipt(invoice, cartManage);
    }

    public void Open()
    {
        //ModifyProduct item1 = new ModifyProduct(ProductService.ProductService.GetCoke(), 4);
        //ModifyProduct item2 = new ModifyProduct(ProductService.ProductService.GetSparklingWater(), 2);
        //ModifyProduct item3 = new ModifyProduct(ProductService.ProductService.GetBread44(), 5);
        //ModifyProduct item4 = new ModifyProduct(ProductService.ProductService.GetBread97(), 5);
        //ModifyProduct item5 = new ModifyProduct(ProductService.ProductService.GetTunaRiceBall(), 5);
        //ModifyProduct item6 = new ModifyProduct(ProductService.ProductService.GetCheeseRiceBall(), 5);
        //ModifyProduct item7 = new ModifyProduct(ProductService.ProductService.GetMeijiMilk225ML(), 5);
        //ModifyProduct item8 = new ModifyProduct(ProductService.ProductService.GetMeijiMilk450ML(), 5);
        //ModifyProduct item9 = new ModifyProduct(ProductService.ProductService.GetLactasoyMilk225ML(), 5);
        //ModifyProduct item10 = new ModifyProduct(ProductService.ProductService.GetLactasoyMilk450ML(), 5);

        //_invoice.Add(item1);
        //_invoice.Add(item2);
        //_invoice.Add(item3);
        //_invoice.Add(item4);
        //_invoice.Add(item5);
        //_invoice.Add(item6);
        //_invoice.Add(item7);
        //_invoice.Add(item8);
        //_invoice.Add(item9);
        //_invoice.Add(item10);

        CancellationTokenSource source = new CancellationTokenSource();

        while (!source.IsCancellationRequested)
        {
            Console.Clear();

            int index = 1;
            foreach (var item in _cartManage.productNames)
            {
                Console.WriteLine($"{index++}: {item.Value.Name} {item.Value.Price:C}");
            }
            Console.WriteLine();
            Console.Write("input product number: ");

            if (int.TryParse(Console.ReadLine(), out int selectedNumber))
            {
                var selectedProduct = _cartManage.productNames[selectedNumber];
                Console.Write($"You selected: {selectedProduct.Name} {selectedProduct.Price:C}, please enter quantity: ");

                if (int.TryParse(Console.ReadLine(), out int productQuantity))
                {
                    ModifyProduct item = new ModifyProduct(selectedProduct, productQuantity);
                    _invoice.Add(item);
                }
                else
                {
                    Console.WriteLine("Invalid input. Please enter a valid number.");
                }
            }
            else
            {
                Console.WriteLine("Invalid input. Please enter a valid number.");
            }

            Console.WriteLine();
            Console.WriteLine("Make purchase?\n1: Yes\n0: No");
            var choose = Console.ReadLine();

            switch (choose)
            {
                case "0":
                    break;

                case "1":
                    source.Cancel();
                    break;

                default:
                    Console.WriteLine("Invalid choice. Try again.");
                    break;
            }
        }

        Console.Clear();
        _receipt.ShowReceipt();
        Console.ReadKey();
    }
}