﻿using discountexercise.AddProduct;
using DiscountLibrary;
using DiscountLibrary.Category;

namespace discountexercise;

internal class Program
{
    private static Mall _mall = new Mall(new Invoice(new List<ModifyProduct>()), new CartManage());

    private static void Main(string[] args)
    {
        _mall.Open();

        //CancellationTokenSource source = new CancellationTokenSource();

        //bool a = false;
        //ChangeBool(a);
        //Console.WriteLine($"source before {source.IsCancellationRequested}");
        //ChangeRef(source);

        //Console.WriteLine(a);
        //Console.WriteLine($"source cancel {source.IsCancellationRequested}");
    }

    //private static void ChangeBool(bool item)
    //{
    //    item = true;
    //}  
    
    //private static void ChangeRef(CancellationTokenSource item)
    //{
    //    item.Cancel();
    //}
}