﻿using System;

namespace ProductService
{
    public class Product : IEquatable<Product>
    {
        public Product(string sku)
        {
            this.Sku = sku;
        }

        public string Name { get; internal set; }
        public decimal Price { get; internal set; }
        public string Sku { get; }

        internal Product SetName(string name)
        {
            this.Name = name;
            return this;
        }

        internal Product SetPrice(decimal price)
        {
            this.Price = price;
            return this;
        }

        public bool Equals(Product other)
        {
            if (other == null) return false;
            return Sku == other.Sku;
        }

        public override bool Equals(object obj)
        {
            if (obj is Product product)
            {
                return Equals(product);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Sku != null ? Sku.GetHashCode() : 0;
        }
    }
}