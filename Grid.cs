﻿namespace discountexercise;

public class Grid
{
    public int boxWidth = 97;

    public void PrintGrid(string col1, string col2, string col3, string col4)
    {
        int col1Width = 40;
        int col2Width = 20;
        int col3Width = 16;
        int col4Width = 16;

        Console.WriteLine($"|{col1.PadRight(col1Width)}|{col2.PadRight(col2Width)}|{col3.PadRight(col3Width)}|{col4.PadRight(col4Width)}|");
    }

    public void PrintLine(string content)
    {
        Console.WriteLine($"|{content.PadRight(boxWidth - 2)}|");
    }
}