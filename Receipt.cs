﻿using discountexercise;
using discountexercise.AddProduct;
using DiscountLibrary;

internal class Receipt
{
    private Grid _grid;
    private Invoice _invoice;
    private CartManage _cartManage;

    public Receipt(Invoice invoice, CartManage cartManage)
    {
        _invoice = invoice;
        _cartManage = cartManage;
        _grid = new Grid();
    }

    public void ShowReceipt()
    {
        decimal totalAmount = _invoice.GetTotal();
        decimal netAmount = _invoice.Discount.GetNetAmount();
        decimal discount = _invoice.Discount.GetDiscountAmount();

        string line = new string('-', _grid.boxWidth);

        Console.WriteLine(line);
        _grid.PrintLine("RECEIPT");
        Console.WriteLine(line);
        _grid.PrintGrid("Item", "Quantity", "Unit Price", "Total Price");
        Console.WriteLine(line);

        //print product details
        if (_invoice.CartList.Any(a => a.Quantity > 0))
        {
            _invoice.CartList.ForEach(f =>
            {
                string col1 = f.Name;
                string col2 = $"{f.Quantity}";
                string col3 = $"{f.Price:C}";
                string col4 = $"{f.Quantity * f.Price:C}";

                _grid.PrintGrid(col1, col2, col3, col4);
            });
            Console.WriteLine(line);
        }

        _grid.PrintGrid("Total Amount", "", "", $"{totalAmount:C}");
        Console.WriteLine(line);

        //print discount
        foreach (var item in _invoice.Discount.DiscountRule)
        {
            if (item.DiscountAmount != 0)
            {
                _grid.PrintGrid($"(Discount: {item.DiscountName})", "", "", $"-{item.DiscountAmount:C}");
            }
        }
        if (_invoice.Discount.DiscountRule.Sum(s => s.DiscountAmount) != 0)
        {
            Console.WriteLine(line);
        }

        _grid.PrintGrid("Net Amount", "", "", $"{netAmount:C}");
        Console.WriteLine(line);
    }
}