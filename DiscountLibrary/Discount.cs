﻿using DiscountLibrary.Category;
using DiscountLibrary.DiscountRule;
using System.Collections.Generic;
using System.Linq;

namespace DiscountLibrary
{
    public class Discount
    {
        private List<ModifyProduct> _cartList;
        private List<IDiscountRule> _discountRules;
        public List<IDiscountRule> DiscountRule => _discountRules;

        public Discount(List<ModifyProduct> cartList)
        {
            _cartList = cartList;
            _discountRules = new List<IDiscountRule>();

            SetDefaultRules();
        }

        public void SetDefaultRules()
        {
            var defaultRules = new List<IDiscountRule>
            {
                new DiscountRule1(),
                new DiscountRule2(),
                new DiscountRule3(),
                new DiscountRule4()
            };

            SetRule(defaultRules);
        }

        public void SetRule(IEnumerable<IDiscountRule> rules)
        {
            _discountRules.Clear();
            _discountRules.AddRange(rules);
        }

        private void AddShortSKU()
        {
            foreach (var f in _cartList)
            {               
                f.SetShortSku(f.Sku.Split('-')[0]);
            }
        }

        public decimal GetDiscountAmount()
        {
            AddShortSKU();

            decimal discount = 0m;

            if (_discountRules != null)
            {
                foreach (var item in _discountRules)
                {
                    discount += item.GetDiscount(_cartList);
                }
            }

            return discount;
        }

        public decimal GetNetAmount()
        {
            decimal netAmount = _cartList.Sum(s => s.Price * s.Quantity);
            netAmount -= GetDiscountAmount();

            return netAmount;
        }
    }
}