using DiscountLibrary.Category;
using System.Collections.Generic;
using System.Linq;

namespace DiscountLibrary.DiscountRule
{
    public class DiscountRule4 : IDiscountRule
    {
        private decimal _discount = 0;
        private decimal _totalPriceForRebate = 5000m;
        private decimal _rebateDiscountPrice = 500m;

        public decimal DiscountAmount => _discount;
        public string DiscountName => "Rebate";

        public DiscountRule4()
        { }

        public decimal GetDiscount(List<ModifyProduct> cartList)
        {
            var result = cartList.Sum(s => s.Price * s.Quantity);

            if (result >= _totalPriceForRebate)
            {
                _discount = _rebateDiscountPrice;
                return _discount;
            }

            return _discount;
        }
    }
}