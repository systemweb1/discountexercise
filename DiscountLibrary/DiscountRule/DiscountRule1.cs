using DiscountLibrary.Category;
using System.Collections.Generic;
using System.Linq;

namespace DiscountLibrary.DiscountRule
{
    public class DiscountRule1 : IDiscountRule
    {
        //feild
        private decimal _discount = 0;
        private static int _productThreePcs = 3;
        private static int _productTwoPcs = 2;
        private static decimal _discountForTwoPcs = 0.1m;
        private static decimal _discountForTheePcs = 0.2m;

        //properties
        public string DiscountName => "Baverage";

        public decimal DiscountAmount => _discount;

        //constructor
        public DiscountRule1()
        { }

        private static List<ModifyProduct> SetBeverageExcludeMeiji(List<ModifyProduct> cartList)
        {
            var beverageList = cartList.Where(w =>
               w.ShortSku.ToLower().Contains("happy")
               || w.ShortSku.ToLower().Contains("milk")
               || w.ShortSku.ToLower().Contains("fmcp")
               || w.ShortSku.ToLower().Contains("rb")
           ).ToList();

            List<ModifyProduct> beverageExcludeMeiji = beverageList.Where(w => !w.Name.Contains("Meiji") && !w.ShortSku.ToLower().Contains("rb")).ToList();

            return beverageExcludeMeiji;
        }

        public decimal GetDiscount(List<ModifyProduct> cartList)
        {
            List<ModifyProduct> beverageExcludeMeiji = SetBeverageExcludeMeiji(cartList);
            _discount = beverageExcludeMeiji.Select(s => CalculateDiscount(s)).Sum();

            return _discount;
        }

        private static decimal CalculateDiscount(ModifyProduct item)
        {
            int quantity = item.Quantity;
            decimal discount = 0m;
            while (quantity > 0)
            {
                if (quantity >= _productThreePcs)
                {
                    discount += _discountForTheePcs * (item.Price * _productThreePcs); // 20% discount for every 3 items
                    quantity -= _productThreePcs;
                }
                else if (quantity >= _productTwoPcs)
                {
                    discount += _discountForTwoPcs * (item.Price * _productTwoPcs); // 10% discount for every 2 items
                    quantity -= _productTwoPcs;
                }
                else
                {
                    quantity--;
                }
            }

            return discount;
        }
    }
}