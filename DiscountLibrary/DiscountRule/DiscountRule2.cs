using DiscountLibrary.Category;
using System.Collections.Generic;
using System.Linq;

namespace DiscountLibrary.DiscountRule
{
    public class DiscountRule2 : IDiscountRule
    {
        private decimal _discount = 0;
        private const decimal _discountPercentage = 0.5m;
        private const int _itemsPerDiscountGroup = 2;

        public string DiscountName => "Bread";
        public decimal DiscountAmount => _discount;

        public DiscountRule2()
        { }

        public decimal GetDiscount(List<ModifyProduct> cartList)
        {
            var breadList = cartList.Where(w => w.ShortSku.ToLower().Contains("bd")).ToList();
            var result = breadList.SelectMany(item => Enumerable.Repeat(item.Price, item.Quantity));

            _discount = CalculateDiscount(result);
            return _discount;
        }

        private decimal CalculateDiscount(IEnumerable<decimal> listFilter)
        {
            int breadCount = listFilter.Count();

            return listFilter
                .OrderBy(o => o)
                .Take(breadCount % _itemsPerDiscountGroup == 0
                    ? breadCount / _itemsPerDiscountGroup
                    : (breadCount - 1) / _itemsPerDiscountGroup)
                .Sum(s => s) * _discountPercentage;
        }
    }
}