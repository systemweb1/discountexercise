using DiscountLibrary.Category;
using System.Collections.Generic;
using System.Linq;

namespace DiscountLibrary.DiscountRule
{
    public class DiscountRule3 : IDiscountRule
    {
        private List<ModifyProduct> _cartList;
        private decimal _discount = 0m;

        private Dictionary<string, decimal> _promotions = new Dictionary<string, decimal>
        {
            { "200_200", 100m },
            { "200_300", 100m },
            { "300_300", 200m }
        };

        public decimal DiscountAmount => _discount;
        public string DiscountName => "Promotion";

        public DiscountRule3()
        { }

        public decimal GetDiscount(List<ModifyProduct> cartList)
        {
            _cartList = cartList;
            return _discount = ApplySandwichAndMilkDiscount();
        }

        private List<ModifyProduct> IncludeSandwichAndMeiji()
        {
            return _cartList.Where(w => w.ShortSku.ToLower().Contains("rb") || w.Name.ToLower().Contains("meiji")).ToList();
        }

        private static IEnumerable<decimal> GetProduct(IEnumerable<ModifyProduct> promotionList)
        {
            return promotionList.OrderByDescending(o => o.Price)
                                .SelectMany(s => Enumerable.Repeat(s.Price, s.Quantity));
        }

        private decimal ApplySandwichAndMilkDiscount()
        {
            var promotionList = IncludeSandwichAndMeiji();
            IEnumerable<decimal> filterSandwiches = GetProduct(promotionList.Where(w => w.ShortSku.ToLower().Contains("rb")).ToList());
            IEnumerable<decimal> filterMilks = GetProduct(promotionList.Where(w => w.Name.ToLower().Contains("meiji")).ToList());

            var SandCount = filterSandwiches.Where(w => w == filterSandwiches.Max()).Count();
            var MilkCount = filterMilks.Where(w => w == filterMilks.Max()).Count();

            var prepareSandwich = filterSandwiches.Skip(SandCount > MilkCount ? 1 : 0);

            return prepareSandwich.Zip(
                    filterMilks,
                    (sandwich, milk) =>
                    (
                        sandwich, //sandwich price
                        milk, //milk price
                        Result: _promotions.TryGetValue($"{sandwich}_{milk}", out decimal promotionValue) ? promotionValue : 0m //discount price
                    )
                )
                .Sum(s => s.Result);
        }
    }
}