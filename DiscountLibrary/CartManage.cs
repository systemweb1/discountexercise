using ProductService;
using System.Collections.Generic;
using static ProductService.ProductService;

namespace discountexercise.AddProduct
{
    public class CartManage
    {
        public Dictionary<int, Product> productNames = new Dictionary<int, Product>
        {
            { 1, GetCoke() },
            { 2, GetLactasoyMilk225ML() },
            { 3, GetLactasoyMilk450ML() },
            { 4, GetMeijiMilk225ML() },
            { 5, GetMeijiMilk450ML() },
            { 6, GetSparklingWater() },
            { 7, GetCheeseRiceBall() },
            { 8, GetBread97() },
            { 9, GetBread44() },
            { 10, GetTunaRiceBall() }
        };
    }
}