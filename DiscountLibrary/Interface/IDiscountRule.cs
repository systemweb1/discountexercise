using DiscountLibrary.Category;
using System.Collections.Generic;

namespace DiscountLibrary
{
    public interface IDiscountRule
    {
        decimal GetDiscount(List<ModifyProduct> cartList);

        decimal DiscountAmount { get; }
        string DiscountName { get; }
    }
}