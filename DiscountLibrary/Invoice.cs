using DiscountLibrary.Category;
using System.Collections.Generic;
using System.Linq;

namespace DiscountLibrary
{
    public class Invoice
    {
        private static List<ModifyProduct> _cartList;
        private Discount _discount;

        public List<ModifyProduct> CartList => _cartList;
        public Discount Discount => _discount;

        public Invoice(List<ModifyProduct> cartList)
        {
            _cartList = cartList;
            _discount = new Discount(cartList);
        }

        public decimal GetTotal()
        {
            decimal totalAmount = _cartList.Sum(s => s.Quantity * s.Price);
            return totalAmount;
        }

        public void Add(ModifyProduct product)
        {
            var item = _cartList.FirstOrDefault(f => f.Sku.Equals(product.Name));
            if (item != null)
            {
                item.SetQuantity(product.Quantity);
            }
            else
            {
                _cartList.Add(product);
            }
        }
    }
}