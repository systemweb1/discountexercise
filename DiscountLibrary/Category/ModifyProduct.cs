using ProductService;
using System.Collections.Generic;

namespace DiscountLibrary.Category
{
    public class ModifyProduct
    {
        private readonly Product _items;
        private readonly IEnumerable<Product> _itemsList;

        public ModifyProduct(Product products, int quantity)
        {
            _items = products;
            Quantity = quantity;
        }

        public ModifyProduct(IEnumerable<Product> products)
        {
            _itemsList = products;
        }

        public string Name => _items.Name;
        public decimal Price => _items.Price;
        public string Sku => _items.Sku;
        public int Quantity { get; private set; }
        public string ShortSku { get; private set; }

        public string SetShortSku(string shortSku) => ShortSku = shortSku;

        public void SetQuantity(int quantity) => Quantity = quantity;
    }
}