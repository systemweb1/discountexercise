﻿using discountexercise.AddProduct;
using DiscountLibrary;
using DiscountLibrary.Category;
using DiscountLibrary.DiscountRule;
using NUnit.Framework;
using NUnit.Framework.Legacy;
using ProductService;

namespace DiscountLibraryTest.RuleTest
{
    [TestFixture]
    public class RebateTest
    {
        private Invoice _invoice;
        private CartManage _cartManage;

        [SetUp]
        public void Setup()
        {
            _invoice = new Invoice(new List<ModifyProduct>());
            _cartManage = new CartManage();
        }

        [TestCase(new[] { 1, 1, 1, 1, 1, 1, 1, 1 }, 0)]
        public void TestRebateNoDiscount(int[] quantities, decimal expectedDiscount)
        {
            // Arrange
            var pickedUpProducts = GetPickedUpProducts(quantities).GroupBy(g => g).OrderBy(o => o.Count());
            foreach (var item in pickedUpProducts)
            {
                ModifyProduct modifyProduct = new ModifyProduct(item.Key, (int)item.Count());
                _invoice.Add(modifyProduct);
            }
            _invoice.Discount.SetRule(new[] { new DiscountRule4() });

            // Act
            var discount = _invoice.Discount.GetDiscountAmount();

            // Assert
            ClassicAssert.AreEqual(expectedDiscount, discount);
        }

        [TestCase(new[] { 25, 25, 25, 25, 25, 25, 25, 25 }, 500)]
        [TestCase(new[] { 100, 50, 10, 10, 10, 10, 10, 10 }, 500)]
        public void TestRebateWithDiscount(int[] quantities, decimal expectedDiscount)
        {
            // Arrange
            var pickedUpProducts = GetPickedUpProducts(quantities).GroupBy(g => g).OrderBy(o => o.Count());
            foreach (var item in pickedUpProducts)
            {
                ModifyProduct modifyProduct = new ModifyProduct(item.Key, (int)item.Count());
                _invoice.Add(modifyProduct);
            }
            _invoice.Discount.SetRule(new[] { new DiscountRule4() });

            // Act
            var discount = _invoice.Discount.GetDiscountAmount();

            // Assert
            ClassicAssert.AreEqual(expectedDiscount, discount);
        }

        private IEnumerable<Product> GetPickedUpProducts(int[] quantities)
        {
            var products = new[]
            {
                new ProductService.ProductService.Cart(ProductService.ProductService.GetCoke(), (uint)quantities[0]),
                new ProductService.ProductService.Cart(ProductService.ProductService.GetSparklingWater(), (uint)quantities[1]),
                new ProductService.ProductService.Cart(ProductService.ProductService.GetBread44(), (uint)quantities[2]),
                new ProductService.ProductService.Cart(ProductService.ProductService.GetBread97(), (uint)quantities[3]),
                new ProductService.ProductService.Cart(ProductService.ProductService.GetCheeseRiceBall(), (uint)quantities[4]),
                new ProductService.ProductService.Cart(ProductService.ProductService.GetTunaRiceBall(), (uint)quantities[5]),
                new ProductService.ProductService.Cart(ProductService.ProductService.GetMeijiMilk225ML(), (uint)quantities[6]),
                new ProductService.ProductService.Cart(ProductService.ProductService.GetMeijiMilk450ML(), (uint)quantities[7]),
            };
            return ProductService.ProductService.GetProductInPack(products);
        }
    }
}