﻿using discountexercise.AddProduct;
using DiscountLibrary;
using DiscountLibrary.Category;
using DiscountLibrary.DiscountRule;
using NUnit.Framework;
using NUnit.Framework.Legacy;
using ProductService;

namespace DiscountLibraryTest.RuleTest
{
    [TestFixture]
    public class BeverageTest
    {
        private Invoice _invoice;
        private CartManage _cartManage;

        [SetUp]
        public void Setup()
        {
            _invoice = new Invoice(new List<ModifyProduct>());
            _cartManage = new CartManage();
        }

        [TestCase(new[] { 7, 5, 5, 5, 5, 5 }, 600)]
        [TestCase(new[] { 8, 0, 0, 0, 0, 0 }, 140)]
        [TestCase(new[] { 5, 0, 0, 0, 0, 0 }, 80)]
        [TestCase(new[] { 2, 3, 5, 7, 11, 13 }, 1200)]
        public void TestBeverageDiscount(int[] quantities, decimal expectedDiscount)
        {
            // Arrange
            var pickedUpProducts = GetPickedUpProducts(quantities).GroupBy(g => g).OrderBy(o => o.Count());
            foreach (var item in pickedUpProducts)
            {
                ModifyProduct modifyProduct = new ModifyProduct(item.Key, (int)item.Count());
                _invoice.Add(modifyProduct);
            }
            _invoice.Discount.SetRule(new[] { new DiscountRule1() });

            var beverageRule = new DiscountRule1();

            // Act
            var discount = _invoice.Discount.GetDiscountAmount();

            // Assert
            ClassicAssert.AreEqual(expectedDiscount, discount);
        }

        [TestCase(new[] { 1, 1, 1, 1, 1, 1 }, 0)]
        [TestCase(new[] { 0, 0, 0, 0, 0, 0 }, 0)]
        public void TestBeverageNoDiscount(int[] quantities, decimal expectedDiscount)
        {
            // Arrange
            var pickedUpProducts = GetPickedUpProducts(quantities).GroupBy(g => g).OrderBy(o => o.Count());
            foreach (var item in pickedUpProducts)
            {
                ModifyProduct modifyProduct = new ModifyProduct(item.Key, (int)item.Count());
                _invoice.Add(modifyProduct);
            }
            _invoice.Discount.SetRule(new[] { new DiscountRule1() });

            var beverageRule = new DiscountRule1();

            // Act
            var discount = _invoice.Discount.GetDiscountAmount();

            // Assert
            ClassicAssert.AreEqual(expectedDiscount, discount);
        }

        private IEnumerable<Product> GetPickedUpProducts(int[] quantities)
        {
            var products = new[]
            {
                new ProductService.ProductService.Cart(ProductService.ProductService.GetCoke(), (uint)quantities[0]),
                new ProductService.ProductService.Cart(ProductService.ProductService.GetSparklingWater(), (uint)quantities[1]),
                new ProductService.ProductService.Cart(ProductService.ProductService.GetBread44(), (uint)quantities[2]),
                new ProductService.ProductService.Cart(ProductService.ProductService.GetBread97(), (uint)quantities[3]),
                new ProductService.ProductService.Cart(ProductService.ProductService.GetLactasoyMilk225ML(), (uint)quantities[4]),
                new ProductService.ProductService.Cart(ProductService.ProductService.GetLactasoyMilk450ML(), (uint)quantities[5]),
            };
            return ProductService.ProductService.GetProductInPack(products);
        }
    }
}