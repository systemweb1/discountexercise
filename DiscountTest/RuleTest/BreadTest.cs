﻿using discountexercise.AddProduct;
using DiscountLibrary;
using DiscountLibrary.Category;
using DiscountLibrary.DiscountRule;
using NUnit.Framework;
using NUnit.Framework.Legacy;
using ProductService;

namespace DiscountLibraryTest.RuleTest
{
    [TestFixture]
    public class BreadTest
    {
        private Invoice _invoice;
        private CartManage _cartManage;

        [SetUp]
        public void Setup()
        {
            _invoice = new Invoice(new List<ModifyProduct>());
            _cartManage = new CartManage();
        }

        [TestCase(new[] { 1, 23, 5, 5 }, 375)]
        [TestCase(new[] { 0, 2, 8, 0 }, 300)]
        [TestCase(new[] { 4, 53, 0, 8 }, 600)]
        [TestCase(new[] { 1, 7, 2, 3 }, 150)]
        [TestCase(new[] { 5, 7, 11, 13 }, 975)]
        [TestCase(new[] { 5, 2, 17, 19 }, 1425)]
        [TestCase(new[] { 9, 12, 23, 29 }, 2175)]
        public void TestBreadDiscount(int[] quantities, decimal expectedDiscount)
        {
            // Arrange
            var pickedUpProducts = GetPickedUpProducts(quantities).GroupBy(g => g).OrderBy(o => o.Count());
            foreach (var item in pickedUpProducts)
            {
                ModifyProduct modifyProduct = new ModifyProduct(item.Key, (int)item.Count());
                _invoice.Add(modifyProduct);
            }
            _invoice.Discount.SetRule(new[] { new DiscountRule2() });

            // Act
            var discount = _invoice.Discount.GetDiscountAmount();

            // Assert
            ClassicAssert.AreEqual(expectedDiscount, discount);
        }

        [TestCase(new[] { 1, 1, 1, 0 }, 0)] // No discount expected
        [TestCase(new[] { 0, 0, 0, 1 }, 0)] // No discount expected
        public void TestBreadNoDiscount(int[] quantities, decimal expectedDiscount)
        {
            // Arrange
            var pickedUpProducts = GetPickedUpProducts(quantities).GroupBy(g => g).OrderBy(o => o.Count());
            foreach (var item in pickedUpProducts)
            {
                ModifyProduct modifyProduct = new ModifyProduct(item.Key, (int)item.Count());
                _invoice.Add(modifyProduct);
            }
            _invoice.Discount.SetRule(new[] { new DiscountRule2() });

            // Act
            var discount = _invoice.Discount.GetDiscountAmount();

            // Assert
            ClassicAssert.AreEqual(expectedDiscount, discount);
        }

        private IEnumerable<Product> GetPickedUpProducts(int[] quantities)
        {
            var products = new[]
            {
                new ProductService.ProductService.Cart(ProductService.ProductService.GetCoke(), (uint)quantities[0]),
                new ProductService.ProductService.Cart(ProductService.ProductService.GetSparklingWater(), (uint)quantities[1]),
                new ProductService.ProductService.Cart(ProductService.ProductService.GetBread44(), (uint)quantities[2]),
                new ProductService.ProductService.Cart(ProductService.ProductService.GetBread97(), (uint)quantities[3])
            };
            return ProductService.ProductService.GetProductInPack(products);
        }
    }
}